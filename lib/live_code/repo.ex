defmodule LiveCode.Repo do
  use Ecto.Repo,
    otp_app: :live_code,
    adapter: Ecto.Adapters.Postgres

  use Scrivener, page_size: 10
end
