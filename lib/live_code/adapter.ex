defmodule LiveCode.Adapter do
  @moduledoc """
    Module to ger external information by JsonPlaceHolder
  """

  require Logger

  def get_users() do
    url = get_url() <> "/users"
    send_get_request(url)
  end

  def get_posts() do
    url = get_url() <> "/posts"
    send_get_request(url)
  end

  def get_comment_by_post(post_id) do
    url = get_url() <> "/posts/#{post_id}/comments"
    send_get_request(url)
  end

  defp send_get_request(url) do
    #Logger.info "JsonPlaceHolder Request #{url}"
    response = HTTPotion.get url

    parse_response(response)
  end

  defp parse_response(response) do
    case response do
      %HTTPotion.ErrorResponse{} ->
        {:error, response.message}
      _else ->
        case response.status_code do
          200 ->
            #Logger.info "JsonPlaceHolder response #{response.body}"
            {:ok, Poison.decode!(response.body)}
          404 ->
            #Logger.error "JsonPlaceHolder response #{response.body}"
            {:error, Poison.decode!(response.body)}
          _else ->
            #Logger.error "JsonPlaceHolder response #{response.body}"
            body =
              response.body
              |> Poison.decode!
            {:error, body}
        end
    end
  end

  defp get_url() do
    config = Application.get_env(:live_code, :json_place_holder)
    config[:url]
  end

end
