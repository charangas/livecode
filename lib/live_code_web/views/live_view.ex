defmodule LiveCodeWeb.LiveView do
  use LiveCodeWeb, :view

  alias LiveCodeWeb.LiveView

  def render("index.json", %{page: page}) do
    %{info: render_many(page.entries, LiveView, "info.json")}
  end

  def render("info.json", %{live: info}) do
    info
  end

end

