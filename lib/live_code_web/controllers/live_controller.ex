defmodule LiveCodeWeb.LiveController do
  @moduledoc false

  import Plug.Conn
  import Phoenix.Controller
  require Logger

  use LiveCodeWeb, :controller

  alias LiveCode.Adapter

  def get_info(conn, params) do


    case make_json() do
      {:ok, data} ->
        info = LiveCode.Repo.paginate(data, params)

        conn
        |> put_status(200)
        |> Scrivener.Headers.paginate(info)
        |> render("index.json", %{page: info})
      {:error, error} ->
        conn
        |> put_status(404)
        |> json(error)
      _else ->
        conn
        |> put_status(500)
        |> json("Error ")
    end
  end

  def make_json() do
    with {:ok, posts} <- Adapter.get_posts(),
         {:ok, users} <- Adapter.get_users() do

      result =
        Enum.map(posts, fn post ->
          user = get_user(users, post["userId"])
          comments = get_comments(post["id"])

          post
          |> Map.put("user", user)
          |> Map.put("comments", comments)
        end)

      {:ok, result}
    else
      {:error, error} ->
        {:error, error}
    end
  end

  def get_user(_users, post_user_id) when is_nil(post_user_id)  do
    %{}
  end

  def get_user(users, post_user_id)  do
    users
    |> Enum.filter(fn user -> user["id"] == post_user_id end)
    |> List.first()
  end

  defp get_comments(post_id)  when is_nil(post_id) do
    %{}
  end

  defp get_comments(post_id) do
    {:ok, comments} = Adapter.get_comment_by_post(post_id)
    comments
  end

end

# LiveCodeWeb.LiveController.make_json